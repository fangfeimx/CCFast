﻿ function Chart (pageID, divID) {

        //加载css. js.

     pageID = "d78143eb-e3ea-40c6-9bba-ebbbb47737e8";

        var handler = new HttpHandler("BP.WF.HttpHandler.WF_Portal");
        handler.AddPara("PageID", pageID);

     var windows = handler.DoMethodReturnJSON("Home_Init");
   //console.log(windows[5])
     var el = $('#' + divID)
     var table = ''
        for (var i = 0; i < windows.length; i++) {
            (function (i) {
                var item = windows[i]
                
                //console.log(item)
                table += '<div class="item" data-no="' + item.No+'">';
                table += '<div class="item-header"><div class="icon-group"><i class="layui-icon layui-icon-console"></i>';
                table += item.Name + '</div></div><div class="item-body">'
                var Chartid = 'ChartLid' + i;
               
                switch (item.WinDocModel) {
                    case "Html": //html的.
                        table += '<div class="inner-content">'+item.Docs+'</div>' ;
                        break;
                    case "System": //内置的.
                        table += '<div class="inner-content">' + item.Docs + '</div>';
                        break;
                    case "Table": //列表的时候的显示.
                        var data = JSON.parse(item.Docs);
                        // console.log(data);
                        table += '<div class="inner-content"><table class="layui-table">';
                        var startnum = data[0];
                        table += '<thead><tr>';
                        $.each(startnum, function (i) {
                            table += '<th>' + i + '</th>';


                        });
                        table += '</tr></thead>';
                        table += '<tbody>';
                        for (var j = 0; j < data.length; j++) {
                            var col = data[j]
                            table += "<tr>"
                            $.each(startnum, function (i) {

                                table += "<td>" + col[i] + "</td>"
                            });
                            table += "</tr>"
                        }
                        table += "</tbody></table></div>"
                        //el.innerHTML = table
                        break
                    case "HtmlVar": //列表的时候的显示.
                        var data = JSON.parse(item.Docs);
                        

                        table += '<div class="inner-content"><div class="layui-row HtmlVar">';
                        for (var j = 0; j < data.length; j++) {
                            var col = data[j]
                            table += "<div class='layui-col-xs6 layui-col-sm6 layui-col-md4'><span>" + col.Name + "</span>"
                            table += "<strong><font color=" + col.FontColor + ">" + col.Exp0 + "</font></strong></div>"

                        }
                        table += "</div></div>"
                        
                        break
                    default:
                       
                        table += '<div class="chart-content" id="' + Chartid + '" data-no="' + i + '"></div>';
                       
                       //initLineChart(chartDom, item);
                        break
                 
                }
          
                table += '</div></div></div>';
            })(i)

            // var html = "";
            //$(divID).html(html);

     };
 
     $(el).html(table)
   
     $("#chart .chart-content").each(function (index, obj) {
         // console.log($(this).attr("data-no"))
         var elid = $(this).attr("id");
         var el = document.getElementById(elid);
         var myNo = $(this).attr("data-no");
         var item = windows[myNo]       
         switch (item.WinDocModel) {
             case "ChartLine":
                 initLineChart(el, item);
                 return
             case "ChartZZT":
                 initHistogram(el, item);
                 return
             case "ChartPie":
                 initPieChart(el, item);
                 return
             case "ChartRate":
                 initGauge(el, item);
                 return
             case "ChartRing":
                 initAnnular(el, item);
                 return
             default:
                 break;
         }
     });
     BindArea(divID);
}


// todo  需要排序接口
function updateSort(str) {
    // 拿到排序后的id数据
    var handler = new HttpHandler("BP.WF.HttpHandler.WF_Portal");
    handler.AddPara("MyPK", str);
    handler.AddUrlData();
    var data = handler.DoMethodReturnString("Home_DoMove");
    layer.msg(data)
}
function BindArea(el) {
  
    var el = document.getElementById(el);
    var sortable = Sortable.create(el, {
        animation: 150,
        ghostClass: 'blue-background-class',
        dataIdAttr: 'data-no',
        onStart: function (evt) {
            loadingDialog = layer.msg('正在移动...', {
                timeout: 900 * 1000
            })
        },
        onEnd: function (evt) {
            layer.close(loadingDialog)
            var arr = this.toArray();
            console.log(arr);
            updateSort(arr.join(','))
        }
    });
       // var wrapper = document.getElementById('chart');

        /*var sortable = Sortable.create(el, {
            animation: 150,
            ghostClass: 'blue-background-class',
            dataIdAttr: 'data-no',
            onStart: function (  evt) {
               loadingDialog = layer.msg('正在移动...', {
                    timeout: 900 * 1000
                })
            },
            onEnd: function (evt) {
                layer.close(_this.loadingDialog)
                var arr = this.toArray();
                updateSort(arr.join(','))
            }
        });*/
   
}



function initLineChart(el, item) {
  
    var lineChart = echarts.init(el)
   
    var data = JSON.parse(item.Docs);

    var startnum = data[0];
    if (startnum) {
        var inf = [];
        var num = 0;
        $.each(startnum, function (i) {
            if (isNaN(startnum[i])) {
                xAxis = data.map(function (it) {
                    return it[i]
                })
            }
            else {
                inf[num] = {
                    name: i,
                    type: 'line',
                    smooth: true,
                    data: data.map(function (it) {
                        return it[i]
                    })

                }
                num++
            }

        });

        var option = {
            legend: {},
            xAxis: {
                type: 'category',
                data: xAxis
            },
            yAxis: {
                type: 'value'
            },
            tooltip: {
                trigger: 'axis'
            },
            series: inf
        };
        lineChart.setOption(option)
        lineChart.resize();
    }

}
// 初始化饼图
function initPieChart(el, item) {
    var pieChart = echarts.init(el);
    //console.log(pieChart)
    //var name = item.Name
    var data = JSON.parse(item.Docs)

    var jsonKey = [];
    for (var jsonVal in data[0]) {
        jsonKey.push(jsonVal);
    }
    var oldkey = {
        [jsonKey[0]]: "name",
        [jsonKey[1]]: "value",
    };

    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        for (var key in obj) {
            var newKey = oldkey[key];
            if (newKey) {
                obj[newKey] = obj[key];
                delete obj[key];
            }
        }
    }
    //console.log(data);
    var option = {
        tooltip: {
            trigger: 'item'
        },
        series: [{
            //name: name,
            type: 'pie',
            radius: '50%',
            data: data,
            emphasis: {
                itemStyle: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }]
    };
    pieChart.setOption(option);
    pieChart.resize();
}
// 初始化柱状图
function initHistogram(el, item) {
    var hChart = echarts.init(el)
    var data = JSON.parse(item.Docs)
    var startnum = data[0];
    if (startnum) {
        var inf = [];
        var num = 0;
        $.each(startnum, function (i) {
            if (isNaN(startnum[i])) {
                xAxis = data.map(function (it) {
                    return it[i]
                })
            }
            else {
                inf[num] = {
                    name: i,
                    type: 'bar',
                    data: data.map(function (it) {
                        return it[i]
                    })

                }
                num++
            }
        });

        option = {
            tooltip: {},
            legend: {},
            xAxis: [
                {
                    type: 'category',
                    data: xAxis
                }
            ],
            yAxis: [
                {
                    type: 'value',

                }
            ],
            dataZoom: [
                {
                    show: true,
                    start: 94,
                    end: 100
                },

            ],
            series: inf
        };
        hChart.setOption(option);
        hChart.resize();
    }
}
//百分比仪表盘
function initGauge(el, item) {
    var GChart = echarts.init(el);
    var option;
    //console.log(item);
    var num = item.SQLOfFZ / item.SQLOfFM * 100;

    option = {
        series: [{
            type: 'gauge',
            anchor: {
                show: true,
                showAbove: true,
                size: 18,
                itemStyle: {
                    color: '#FAC858'
                }
            },

            progress: {
                show: true,
                overlap: true,
                roundCap: true
            },
            axisLine: {
                roundCap: true
            },
            data: [{
                value: num.toFixed(2),
                name: item.LabOfRate,
                title: {
                    offsetCenter: ['0%', '75%']
                },
                detail: {
                    offsetCenter: ['0%', '95%']
                }
            },

            ],
            title: {
                fontSize: 12
            },
            detail: {
                width: 40,
                height: 14,
                fontSize: 12,
                color: '#fff',
                backgroundColor: 'auto',
                borderRadius: 3,
                formatter: '{value}%'
            }
        }]
    };


    GChart.setOption(option);
    GChart.resize();
}
//环形
function initAnnular(el, item) {

    var AnnularChart = echarts.init(el);
    var name = item.Name
    var data = JSON.parse(item.Docs)
    var jsonKey = [];
    for (var jsonVal in data[0]) {
        jsonKey.push(jsonVal);
    }
    var oldkey = {
        [jsonKey[0]]: "name",
        [jsonKey[1]]: "value",
    };

    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        for (var key in obj) {
            var newKey = oldkey[key];
            if (newKey) {
                obj[newKey] = obj[key];
                delete obj[key];
            }
        }
    }
    var option = {
        tooltip: {
            trigger: 'item'
        },
        legend: {},
        series: [{
            name: item.Name,
            type: 'pie',
            radius: ['40%', '70%'],
            avoidLabelOverlap: false,
            itemStyle: {
                borderRadius: 10,
                borderColor: '#fff',
                borderWidth: 2
            },
            label: {
                show: false,
                position: 'center'
            },
            emphasis: {
                label: {
                    show: true,
                    fontSize: '14',
                    fontWeight: '500'
                }
            },
            labelLine: {
                show: false
            },
            data: data
        }]
    };
    AnnularChart.setOption(option);

    AnnularChart.resize();
}

